# Modify hosts
A tiny script which adds, modifies or removes ssh host configurations in your ssh config file.
Your general ssh config configurations are prepend to the generated ssh config.

## Defaults
The default settings can be written to `modifyhosts.py`.
~~~
# The User to login to Host, usually the current user
default_user                   = getpass.getuser()

# The path to IdentityFile
default_keypath                = '~/.ssh/id_rsa'

# The SSH config file
default_config                 = '~/.ssh/config'

# A general SSH config file, which will be prepend
default_generalconfig          = '~/.ssh/config.in'

# The path to the json hosts database file
default_hostsfile              = '~/.ssh/hosts.json'

# The Port to login to Host
default_port                   = 22
~~~

## Install
You can install the python egg with:
~~~
python setup.py install
~~~
(provided, you have python-pip installed)

## Usage
Show all options:
~~~
./sshconfig.py --help
Usage: modifyhost.py [OPTIONS] COMMAND [ARGS]...

Options:
  -c, --config TEXT
  -C, --generalconfig TEXT
  -h, --hostsfile TEXT
  --help                    Show this message and exit.

Commands:
  add          Add a host
  list         Lists your hosts
  maintenance  This is maintenance stuff only!
  modify       Modify a host
  remove       Remove a host
~~~

Show the options help:
~~~
./sshconfig.py add --help
Usage: modifyhost.py add [OPTIONS]

  Add a host

Options:
  -a, --alias TEXT     Short name for host
  -h, --hostname TEXT  Hostname or IP address
  -u, --user TEXT      Username to login
  -p, --port INTEGER   SSH port
  -k, --keypath TEXT   Path to identityfile
  --help               Show this message and exit.
~~~
