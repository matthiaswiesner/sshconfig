from setuptools import setup

setup(
    name='sshconfig',
    version='1.0',
    description='Add, change or remove ssh config host entries',
    author='Matthias Wiesner',
    author_email='matthias.wiesner@googlemail.com',
    packages=['sshconfig'],
    install_requires=['click'],
    entry_points={
        'console_scripts': ['sshconfig = sshconfig:cli']
    }
)
