#!/usr/bin/env python
import os
import re
import sys
import json
import click
import shutil
import getpass
import logging

'''
Define defaults
'''

# The User to login to Host, usually the current user
default_user = getpass.getuser()

# The path to IdentityFile
default_keypath = '~/.ssh/id_rsa'

# The SSH config file
default_config = '~/.ssh/config'

# A general SSH config file, which will be prepend
default_generalconfig = '~/.ssh/config.in'

# The path to the json hosts database file
default_hostsfile = '~/.ssh/hosts.json'

# The Port to login to Host
default_port = 22

# set logging
log = logging.getLogger(__name__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setLevel(logging.DEBUG)
log.addHandler(out_hdlr)
log.setLevel(logging.DEBUG)


def _input_values(host):
    hostname = input("{:<30}".format(f"Hostname [{host['hostname']}]: ")) or host['hostname']
    user = input("{:<30}".format(f"User [{host['user']}]: ")) or host['user']
    port = input("{:<30}".format(f"Port [{host['port']}]: ")) or host['port']
    keypath = input("{:<30}".format(f"Keypath [{host['keypath']}]: ")) or host['keypath']
    return hostname, user, port, keypath


@click.group()
@click.option('--config', '-c', default=default_config)
@click.option('--generalconfig', '-C', default=default_generalconfig)
@click.option('--hostsfile', '-h', default=default_hostsfile)
@click.pass_context
def cli(ctx, config, generalconfig, hostsfile):
    class ModHost(object):
        pass
    ctx.obj = ModHost()
    ctx.obj.config = os.path.expanduser(config)
    ctx.obj.generalconfig = os.path.expanduser(generalconfig)
    ctx.obj.hostsfile = os.path.expanduser(hostsfile)
    ctx.obj.template = '''
Host {alias}
    User {user}
    HostName {hostname}
    IdentityFile {keypath}
    Port {port}
'''

    if not os.path.exists(ctx.obj.hostsfile):
        open(ctx.obj.hostsfile, 'a').close()
        ctx.obj.hosts = dict()

        log.info("This is your first run, I create a copy of your current config file")  # nopep8
        shutil.copyfile(ctx.obj.config, ctx.obj.config + '.original')
        if not os.path.exists(ctx.obj.generalconfig):
            if click.confirm('Should I prepend your current config to the generated config?', abort=True):  # nopep8
                shutil.copyfile(ctx.obj.config, ctx.obj.generalconfig)
    else:
        with open(ctx.obj.hostsfile, 'r') as db:
            ctx.obj.hosts = json.load(db)


@cli.command(help='Show host config for a host')
@click.argument('alias')
@click.pass_context
def show(ctx, alias):
    if alias in ctx.obj.hosts:
        host = ctx.obj.hosts[alias]
        log.info(
            ctx.obj.template.format(
            alias=alias,
            hostname=host['hostname'],
            user=host['user'],
            port=host['port'],
            keypath=host['keypath']
        ))
    else:
        log.error('No host found with this name!')


@cli.command(help='Lists your hosts')
@click.option('--verbose', '-v', is_flag=True, help='Show all')
@click.pass_context
def list(ctx, verbose):
    for alias in sorted(ctx.obj.hosts.keys()):
        host = ctx.obj.hosts[alias]
        if verbose:
            log.info(
                ctx.obj.template.format(
                alias=alias,
                hostname=host['hostname'],
                user=host['user'],
                port=host['port'],
                keypath=host['keypath']
            ))
        else:
            log.info('Host {alias}'.format(alias=alias))


@cli.command(help='Add a host')
@click.argument('alias')
@click.option('--hostname', '-h', default='', help='Hostname or IP address')  # nopep8
@click.option('--user', '-u', default=default_user, help='Username to login')
@click.option('--port', '-p', default=default_port, help='SSH port')
@click.option('--keypath', '-k', default=default_keypath, help='Path to identityfile')  # nopep8
@click.pass_context
def add(ctx, alias, hostname, user, port, keypath):
    if alias not in ctx.obj.hosts:
        host = {
            'hostname': hostname,
            'user': user,
            'port': port,
            'keypath': os.path.expanduser(keypath)    
        }

        while not all([hostname, user, port, keypath]):
            hostname, user, port, keypath = _input_values(host)
            if all([hostname, user, port, keypath]):
                break
            print("\nYou have to fill out all settings\n")

        ctx.obj.hosts[alias] = {
            'hostname': hostname,
            'user': user,
            'port': port,
            'keypath': keypath
        }
        _write_hosts(ctx)
        _write_config(ctx)
    else:
        host = ctx.obj.hosts[alias]
        log.warn('You have already a host configured with this name:')
        log.info(
            ctx.obj.template.format(
            alias=alias,
            hostname=host['hostname'],
            user=host['user'],
            port=host['port'],
            keypath=host['keypath']
        ))


@cli.command(help='Modify a host')
@click.argument('alias')
@click.option('--hostname', '-h', default=None)
@click.option('--user', '-u', default=None)
@click.option('--port', '-p', default=None)
@click.option('--keypath', '-k', default=None)
@click.pass_context
def modify(ctx, alias, hostname, user, port, keypath):
    if alias in ctx.obj.hosts:
        host = ctx.obj.hosts[alias]

        while not all([hostname, user, port, keypath]):
            hostname, user, port, keypath = _input_values(host)
            if all([hostname, user, port, keypath]):
                break
            print("\nYou have to fill out all settings\n")

        ctx.obj.hosts[alias] = {
            'hostname': hostname,
            'user': user,
            'port': port,
            'keypath': keypath
        }
        _write_hosts(ctx)
        _write_config(ctx)
    else:
        log.error('No host found with this name!')


@cli.command(help='Change alias')
@click.argument('alias')
@click.option('--newalias', '-n', prompt='Set the new alias')
@click.pass_context
def change_alias(ctx, alias, newalias):
    if alias in ctx.obj.hosts:
        host = ctx.obj.hosts[alias]
        if newalias in ctx.obj.hosts:
            host = ctx.obj.hosts[newalias]
            log.warn('There is already a host with this new alias!')
            log.info(
                ctx.obj.template.format(
                alias=alias,
                hostname=host['hostname'],
                user=host['user'],
                port=host['port'],
                keypath=host['keypath']
            ))
            return
        del ctx.obj.hosts[alias]
        ctx.obj.hosts[newalias] = host
        _write_hosts(ctx)
        _write_config(ctx)
    else:
        log.error('No host found with this name!')


@cli.command(help='Remove a host')
@click.argument('alias')
@click.pass_context
def remove(ctx, alias):
    if alias in ctx.obj.hosts:
        del ctx.obj.hosts[alias]
        _write_hosts(ctx)
        _write_config(ctx)
    else:
        log.error('No host found with this name!')


@cli.command(help='This is maintenance stuff only!')
@click.pass_context
def maintenance(ctx):
    '''
    Implement maintenance stuff, i.e. add field for all hosts
    '''
    pass


def _write_hosts(ctx):
    with open(ctx.obj.hostsfile, 'w') as db:
        db.write(json.dumps(ctx.obj.hosts))


def _write_config(ctx):
    generalconfig = ''
    if os.path.exists(ctx.obj.generalconfig):
        with open(ctx.obj.generalconfig, 'r') as cfg:
            generalconfig = cfg.read()

    shutil.copyfile(ctx.obj.config, ctx.obj.config + '.backup')
    with open(ctx.obj.config, 'w') as cfg:
        cfg.write(generalconfig)
        for alias, host in ctx.obj.hosts.items():
            cfg.write(ctx.obj.template.format(
                alias=alias,
                hostname=host['hostname'],
                user=host['user'],
                port=host['port'],
                keypath=host['keypath']
                ))

if __name__ == '__main__':
    cli()  # pylint: disable=E1120
